﻿using System;
using System.IO;

namespace Zylab.Interview.BinStorage
{
    /// <summary>
    /// This interface represents a concrete implementation of the BinaryStorage.
    /// So the Binary Storage interface can be modified on its own as well as different implementations can to the same thing.
    /// But at the current moment it just mirrors the <see cref="IBinaryStorage"/> IBinaryStorage interface.
    /// </summary>
    public interface IBinaryStorageConcrete : IDisposable
    {
        void Add(string key, Stream data, StreamInfo parameters);
        bool Contains(string key);
        Stream Get(string key);
    }
}