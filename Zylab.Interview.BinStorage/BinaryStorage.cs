﻿using System;
using System.IO;
using Zylab.Interview.BinStorage.FileSystem;

namespace Zylab.Interview.BinStorage
{
    public class BinaryStorage : IBinaryStorage
    {
        private readonly IBinaryStorageConcrete _implementation;

        public BinaryStorage(StorageConfiguration configuration, Func<StorageConfiguration, IBinaryStorageConcrete> factoryFn)
        {
            if(configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if(factoryFn == null)
                throw new ArgumentNullException(nameof(factoryFn));

            _implementation = factoryFn(configuration);
        }

        public BinaryStorage(IBinaryStorageConcrete implementation)
        {
            if(implementation == null)
                throw new ArgumentNullException(nameof(implementation));

            _implementation = implementation;
        }

        public BinaryStorage(StorageConfiguration configuration)
        {
            if(configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            _implementation = new BinaryStorageFsImpl(configuration);
        }

        public void Add(string key, Stream data, StreamInfo parameters)
        {
            _implementation.Add(key, data, parameters);
        }

        public Stream Get(string key)
        {
            return _implementation.Get(key);
        }

        public bool Contains(string key)
        {
            return _implementation.Contains(key);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _implementation?.Dispose();
            }
        }
    }
}
