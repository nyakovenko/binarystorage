﻿# Binary Storage

This assembly contains an implementation of the Binary storage written in c# 6 using .NET framework 4.5.
Binary storage is a write once read many data structure,
which provides a persistent storage for binary content.
Once added to the storage the data cannot be modified through storage API.

Binary storage consists of two logical parts *Index* and *Data Storage*.
*Data storage* - is the actual abstract storage where all binary streams are stored.
*Index* - is a data structure that is used to associate keys with streams by means of references.
It may also store some extra information if it is required for a normal operation of the storage.

Binary Storage is structured in a such way that the default implementation can
be easily replaced with the custom one.

Binary Storage is a thread-safe and supports multi-threaded access (both read & write operations).

## Library Structure
* Root folder of the assembly contains all mandatory contracts and models required for binary storage.
* **FileSystem** folder contains core classes which are related to file system based implementation of the Binary storage.
* **Common** folder on the other hand contains more generic purpose classes which can be re-used in other possible implementations, such as hash provider, compression provider or cache.


## Default Implementation
The default implementation of the Binary storage uses File System to save 
both *Data Storage* and *Index*. It also involves an in-memory cache to store frequently adressed indexes.
Please see files from **FileSystem** folder mentioned below for detailed information.
```csharp 
BinaryStorageFsImpl.cs
BinaryStorageFsIndex.cs
```


## Features
* Binary storage does not have any 3rd party dependencies. The only 3rd party library in the solution is NUnit which has been chosen as a replacement for MS tests due to a better API.
See https://github.com/nunit/nunit for more details about this library.
* Binary storage in its current implementation does not require any additional software installed.
* Current implementation of Binary Storage uses streams all over the place to ensure a minimum usage of RAM. It also allows this data structure to successfully work with large files.
* Current Index implementation has a compact representation on a disk based on how git stores its object references, which provides a decent speed and an easy way to locate a record while accessing index records.
* Thread-safe Add and Read operations.
* Supports parallel addition of new files.