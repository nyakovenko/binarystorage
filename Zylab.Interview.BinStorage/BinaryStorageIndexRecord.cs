﻿using System;
using System.IO.Compression;

namespace Zylab.Interview.BinStorage
{
    [Serializable]
    public class BinaryStorageIndexRecord
    {
        /// <summary>
        /// Hash summ of the the file to which this index record relates to.
        /// </summary>
        public byte[] Hash { get; set; }

        /// <summary>
        /// The zero-based byte offset in the storage file at which to begin storing the data read from the current file.
        /// </summary>
        public long Offset { get; set; }

        /// <summary>
        /// Final length of the file in bytes.
        /// </summary>
        public long Length { get; set; }

        /// <summary>
        /// Original length of the file in bytes.
        /// </summary>
        public long OriginLength { get; set; }

        /// <summary>
        /// The level of compression which has been applied to the file.
        /// </summary>
        public CompressionLevel CompressionLevel { get; set; }
    }
}
