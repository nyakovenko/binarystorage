﻿using System;

namespace Zylab.Interview.BinStorage
{
    public interface IBinaryStorageIndex : IDisposable
    {
        /// <summary>
        /// Read index from index storage
        /// </summary>
        /// <param name="key">Key which represents a file paired with the particular index record</param>
        /// <returns>Index record object with data required to successfully process storage operations</returns>
        BinaryStorageIndexRecord Read(string key);

        /// <summary>
        /// Write index record object associated with the provided file key to the index storage
        /// </summary>
        /// <param name="key">Key which represents a file paired with the particular index record</param>
        /// <param name="record">Index record object with data required to successfully process storage operations</param>
        void Write(string key, BinaryStorageIndexRecord record);

        /// <summary>
        /// Check whether an index record with provided file key exists in the index storage
        /// </summary>
        /// <param name="key">Key which represents a file paired with the particular index record</param>
        /// <returns></returns>
        bool Contains(string key);
    }
}