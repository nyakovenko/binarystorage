﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace Zylab.Interview.BinStorage.Common
{
    public class BinaryStorageCompressionProvider : IBinaryStorageCompressionProvider
    {
        public static Lazy<IBinaryStorageCompressionProvider> Default => new Lazy<IBinaryStorageCompressionProvider>(() => new BinaryStorageCompressionProvider(),
                   LazyThreadSafetyMode.PublicationOnly);

        public Stream GetCompressionStream(Stream data, CompressionLevel compressionLevel = CompressionLevel.Optimal)
        {
            return new DeflateStream(data, compressionLevel, leaveOpen: true);
        }

        public Stream GetDecompressionStream(Stream data)
        {
            return new DeflateStream(data, CompressionMode.Decompress, leaveOpen: false);
        }
    }
}
