﻿using System;
using System.Runtime.Caching;
using System.Threading;

namespace Zylab.Interview.BinStorage.Common
{
    public class BinaryStorageCache : IBinaryStorageCache
    {
        public static Lazy<IBinaryStorageCache> Default => new Lazy<IBinaryStorageCache>(() => new BinaryStorageCache(), LazyThreadSafetyMode.PublicationOnly);

        private const string BinaryStorageCacheName = "bs-cache";

        private TimeSpan DefaultStoreTime => new TimeSpan(0, 5, 0);

        private readonly MemoryCache _cache;

        public BinaryStorageCache()
        {
            _cache = new MemoryCache(BinaryStorageCacheName);
        }

        public T GetOrAdd<T>(string key, Func<T> factoryFn, TimeSpan? time = null)
        {
            var itemStoreTime = time ?? DefaultStoreTime;

            var policy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTimeOffset.Now.Add(itemStoreTime),
                Priority = CacheItemPriority.Default
            };

            var entry = _cache.AddOrGetExisting(key, factoryFn(), policy);

            return (T) entry;
        }

        public T Get<T>(string key)
        {
            return (T)_cache.Get(key);
        }

        public bool Contains(string key)
        {
            return _cache.Contains(key);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _cache?.Dispose();
            }
        }
    }
}
