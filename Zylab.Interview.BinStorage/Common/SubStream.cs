﻿using System;
using System.IO;
using Zylab.Interview.BinStorage.Properties;

namespace Zylab.Interview.BinStorage.Common
{
    class SubStream : Stream
    {
        private readonly long _length;
        private Stream _baseStream;
        private long _position;
        private const int BufferSize = 8192;

        public SubStream(Stream baseStream, long offset, long length)
        {
            if (baseStream == null)
                throw new ArgumentNullException(nameof(baseStream));

            if (!baseStream.CanRead)
                throw new ArgumentException(Resources.ExceptionsResource_StreamDoesNotSupportReading);

            if (offset < 0)
                throw new ArgumentOutOfRangeException(nameof(offset));

            if(length < 0)
                throw new ArgumentOutOfRangeException(nameof(length));

            _baseStream = baseStream;
            _length = length;

            if (baseStream.CanSeek)
            {
                baseStream.Seek(offset, SeekOrigin.Current);
            }
            else
            {
                ManualStreamSeek(baseStream, offset);
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            EnsureStreamNotDisposed();

            var remaining = _length - _position;

            if (remaining <= 0)
                return 0;

            if (remaining < count)
                count = (int)remaining;

            var read = _baseStream.Read(buffer, offset, count);

            _position += read;

            return read;
        }

        public override long Length
        {
            get { EnsureStreamNotDisposed(); return _length; }
        }

        public override bool CanRead
        {
            get { EnsureStreamNotDisposed(); return true; }
        }

        public override bool CanWrite
        {
            get { EnsureStreamNotDisposed(); return false; }
        }

        public override bool CanSeek
        {
            get { EnsureStreamNotDisposed(); return false; }
        }

        public override long Position
        {
            get { EnsureStreamNotDisposed(); return _position; }
            set { throw new NotSupportedException(); }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Flush()
        {
            EnsureStreamNotDisposed();
            _baseStream.Flush();
        }


        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                if (_baseStream != null)
                {
                    try
                    {
                        _baseStream.Dispose();
                    }
                    catch
                    { 
                    }

                    _baseStream = null;
                }
            }
        }

        private void EnsureStreamNotDisposed()
        {
            if (_baseStream == null)
                throw new ObjectDisposedException(GetType().Name);
        }

        private static void ManualStreamSeek(Stream stream, long offset)
        {
            var buffer = new byte[BufferSize];
            while (offset > 0)
            {
                var readBytesCnt = offset < BufferSize ? (int)offset : BufferSize;
                var read = stream.Read(buffer, 0, readBytesCnt);
                offset -= read;
            }
        }
    }
}
