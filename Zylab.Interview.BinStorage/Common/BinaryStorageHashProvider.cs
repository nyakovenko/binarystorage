﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using Zylab.Interview.BinStorage.Properties;

namespace Zylab.Interview.BinStorage.Common
{
    public class BinaryStorageHashProvider : IBinaryStorageHashProvider
    {
        public static Lazy<IBinaryStorageHashProvider> Default => new Lazy<IBinaryStorageHashProvider>(() => new BinaryStorageHashProvider(),
                    LazyThreadSafetyMode.PublicationOnly);

        public byte[] ComputeHash(byte[] data)
        {
            using (var algo = GetHashAlgorithm())
            {
                var hash = algo.ComputeHash(data);
                return hash;
            }
        }

        public byte[] ComputeHash(Stream data)
        {
            using (var algo = GetHashAlgorithm())
            {
                var hash = algo.ComputeHash(data);
                return hash;
            }
        }

        protected virtual HashAlgorithm GetHashAlgorithm(string hashName = "md5")
        {
            var instance = HashAlgorithm.Create(hashName);

            if(instance == null)
                throw new ArgumentException(Resources.ExceptionsResource_UnknowHashName);

            return instance;
        }
    }
}
