﻿using System;

namespace Zylab.Interview.BinStorage.Common
{
    public interface IBinaryStorageCache : IDisposable
    {
        T GetOrAdd<T>(string key, Func<T> factoryFn, TimeSpan? time = null);
        T Get<T>(string key);
        bool Contains(string key);
    }
}