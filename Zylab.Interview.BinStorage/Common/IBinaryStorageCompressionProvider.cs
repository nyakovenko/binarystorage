﻿using System.IO;
using System.IO.Compression;

namespace Zylab.Interview.BinStorage.Common
{
    public interface IBinaryStorageCompressionProvider
    {
        Stream GetCompressionStream(Stream data, CompressionLevel compressionLevel = CompressionLevel.Optimal);
        Stream GetDecompressionStream(Stream data);
    }
}