﻿using System.IO;

namespace Zylab.Interview.BinStorage.Common
{
    public interface IBinaryStorageHashProvider
    {
        byte[] ComputeHash(byte[] data);
        byte[] ComputeHash(Stream data);
    }
}
