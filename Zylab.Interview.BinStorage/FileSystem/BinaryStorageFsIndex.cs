﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using Zylab.Interview.BinStorage.Common;
using Zylab.Interview.BinStorage.Properties;

namespace Zylab.Interview.BinStorage.FileSystem
{
    public class BinaryStorageFsIndex : IBinaryStorageIndex
    {
        public static IBinaryStorageIndex GetDefaultWithCache(string workingFolder, long maxIndexCapacity)
            => new BinaryStorageFsCachedIndex(new BinaryStorageFsIndex(workingFolder, maxIndexCapacity));

        private readonly IBinaryStorageHashProvider _hashProvider;

        private readonly string _storageFilePath;

        private readonly long _maxIndexCapacity;

        private long _indexCapacity;

        public BinaryStorageFsIndex(string workingFolder, long maxIndexCapacity, IBinaryStorageHashProvider hashProvider)
            : this(workingFolder, maxIndexCapacity)
        {
            if(hashProvider == null)
                throw new ArgumentNullException(nameof(hashProvider));

            _hashProvider = hashProvider;
        }

        public BinaryStorageFsIndex(string workingFolder, long maxIndexCapacity)
        {
            if (string.IsNullOrEmpty(workingFolder))
                throw new ArgumentNullException(nameof(workingFolder));

            _storageFilePath = Path.Combine(workingFolder, "objects");
            _storageFilePath.EnsureDirectoryExists();
            _maxIndexCapacity = maxIndexCapacity;

            _indexCapacity = _storageFilePath.GetDirectorySize();

            if (_hashProvider == null)
                _hashProvider = BinaryStorageHashProvider.Default.Value;
        }

        public BinaryStorageIndexRecord Read(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            var indexKey = ComputeIndexKey(key);

            var indexFilePath = ComputeIndexFilePath(indexKey);

            if (!File.Exists(indexFilePath))
                return null;

            BinaryStorageIndexRecord index;
            using (var fs = File.OpenRead(indexFilePath))
            {
                var formatter = new BinaryFormatter();
                index = (BinaryStorageIndexRecord) formatter.Deserialize(fs);
            }

            return index;
        }

        public void Write(string key, BinaryStorageIndexRecord record)
        {
            if (record == null)
                throw new ArgumentNullException(nameof(record));

            var indexKey = ComputeIndexKey(key);

            var indexFilePath = ComputeIndexFilePath(indexKey);

            if (File.Exists(indexFilePath))
                throw new InvalidOperationException(Resources.ExceptionsResource_AddingIndexRecordDuplicate);

            var directory = Path.GetDirectoryName(indexFilePath);

            directory.EnsureDirectoryExists();

            try
            {
                using (var fs = File.Create(indexFilePath))
                using (var ms = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();

                    formatter.Serialize(ms, record);

                    EnsureIndexCapacity(ms.Length);

                    Interlocked.Add(ref _indexCapacity, ms.Length);

                    ms.Seek(0, SeekOrigin.Begin);
                    ms.CopyTo(fs);
                }
            }
            catch (Exception ex)
            {
                if(ex.IsDiskFull())
                    throw new InvalidOperationException(Resources.ExceptionsResource_NotEnoughDiskSpace, ex);

                throw;
            }
        }

        public bool Contains(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            var indexKey = ComputeIndexKey(key);

            var indexFilePath = ComputeIndexFilePath(indexKey);

            return File.Exists(indexFilePath);
        }

        private void EnsureIndexCapacity(long nextIndexRecord = 0)
        {
            if (_maxIndexCapacity == 0)
                return;

            var current = Interlocked.Read(ref _indexCapacity);

            var isExceeded = current + nextIndexRecord >= _maxIndexCapacity;

            if (isExceeded)
                throw new InvalidOperationException(Resources.ExceptionsResource_IndexCapacityExceeded);           
        }

        private string ComputeIndexKey(string key)
        {
            var bytes = Encoding.UTF8.GetBytes(key);
            var hash = _hashProvider.ComputeHash(bytes);
            var indexKey = BitConverter.ToString(hash).Replace("-", String.Empty);
            return indexKey;
        }

        private string ComputeIndexFilePath(string indexKey)
        {
            var folder = new string(indexKey.Take(2).ToArray());
            var fileName = indexKey.Substring(2, indexKey.Length - 2);
            var path = Path.Combine(_storageFilePath, folder, fileName);
            return path;
        }

        public void Dispose()
        {
        }
    }
}
