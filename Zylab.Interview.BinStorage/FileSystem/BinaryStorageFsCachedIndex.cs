﻿using System;
using Zylab.Interview.BinStorage.Common;

namespace Zylab.Interview.BinStorage.FileSystem
{
    public class BinaryStorageFsCachedIndex : IBinaryStorageIndex
    {
        private readonly IBinaryStorageCache _cache;

        private readonly IBinaryStorageIndex _indexStorage;

        public BinaryStorageFsCachedIndex(IBinaryStorageIndex indexStorage, IBinaryStorageCache cache)
        {
            if (indexStorage == null)
                throw new ArgumentNullException(nameof(indexStorage));

            _indexStorage = indexStorage;

            if (cache == null)
                throw new ArgumentNullException(nameof(cache));

            _cache = cache;
        }

        public BinaryStorageFsCachedIndex(IBinaryStorageIndex indexStorage)
        {
            if(indexStorage == null)
                throw new ArgumentNullException(nameof(indexStorage));

            _indexStorage = indexStorage;

            if(_cache == null)
                _cache = BinaryStorageCache.Default.Value;
        }

        public BinaryStorageIndexRecord Read(string key)
        {
            var record = _cache.Get<BinaryStorageIndexRecord>(key);

            record = record ?? _indexStorage.Read(key);

            if (record != null)
            {
                _cache.GetOrAdd(key, () => record);
            }

            return record;
        }

        public void Write(string key, BinaryStorageIndexRecord record)
        {
            _indexStorage.Write(key, record);
            _cache.GetOrAdd(key, () => record);
        }

        public bool Contains(string key)
        {
            if (_cache.Contains(key))
                return true;

            return _indexStorage.Contains(key);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _cache?.Dispose();
                _indexStorage?.Dispose();
            }
        }
    }
}
