﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using Zylab.Interview.BinStorage.Common;
using Zylab.Interview.BinStorage.Properties;

namespace Zylab.Interview.BinStorage.FileSystem
{
    public class BinaryStorageFsImpl : IBinaryStorageConcrete
    {
        private long _position;

        private readonly string _storageFilePath;

        private readonly int _blockSize;

        private readonly long _maxStorageFile;

        private readonly long _compressionThreshold;

        private readonly IBinaryStorageIndex _indexStorage;

        private readonly IBinaryStorageHashProvider _hashProvider;

        private readonly IBinaryStorageCompressionProvider _compressionProvider;

        private readonly ConcurrentDictionary<string, string> _ongoingRecords;

        private readonly string _storageTempFilesFolder;

        public CompressionLevel CompressionLevel { get; set; } = CompressionLevel.Optimal;

        public BinaryStorageFsImpl(StorageConfiguration configuration, IBinaryStorageIndex indexStorage) : this(configuration)
        {
            if (indexStorage == null)
                throw new ArgumentNullException(nameof(indexStorage));

            _indexStorage = indexStorage;
        }

        public BinaryStorageFsImpl(StorageConfiguration configuration, IBinaryStorageHashProvider hashProvider) : this(configuration)
        {
            if (hashProvider == null)
                throw new ArgumentNullException(nameof(hashProvider));

            _hashProvider = hashProvider;
        }

        public BinaryStorageFsImpl(StorageConfiguration configuration, IBinaryStorageIndex indexStorage, IBinaryStorageHashProvider hashProvider, IBinaryStorageCompressionProvider compressionProvider) : this(configuration)
        {
            if (indexStorage == null)
                throw new ArgumentNullException(nameof(indexStorage));

            if (hashProvider == null)
                throw new ArgumentNullException(nameof(hashProvider));

            if(compressionProvider == null)
                throw new ArgumentNullException(nameof(compressionProvider));

            _compressionProvider = compressionProvider;

            _indexStorage = indexStorage;

            _hashProvider = hashProvider;
        }

        public BinaryStorageFsImpl(StorageConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (configuration.CompressionThreshold < 0)
                throw new ArgumentOutOfRangeException(nameof(configuration.CompressionThreshold));

            if (configuration.MaxIndexFile < 0)
                throw new ArgumentOutOfRangeException(nameof(configuration.MaxIndexFile));

            if (configuration.MaxStorageFile < 0)
                throw new ArgumentOutOfRangeException(nameof(configuration.MaxIndexFile));

            if (configuration.BlockSize <= 0)
                throw new ArgumentOutOfRangeException(nameof(configuration.BlockSize));

            if (string.IsNullOrEmpty(configuration.WorkingFolder))
                throw new ArgumentNullException(nameof(configuration.WorkingFolder));

            var storageFolder = Path.Combine(configuration.WorkingFolder, "data");
            storageFolder.EnsureDirectoryExists();

            _storageFilePath = Path.Combine(storageFolder, "storage.bin");

            _storageTempFilesFolder = Path.Combine(storageFolder, "temp");
            _storageTempFilesFolder.EnsureDirectoryExists();

            _maxStorageFile = configuration.MaxStorageFile;

            _compressionThreshold = configuration.CompressionThreshold;

            _blockSize = configuration.BlockSize;

            if (_indexStorage == null)
                _indexStorage = BinaryStorageFsIndex.GetDefaultWithCache(configuration.WorkingFolder, configuration.MaxIndexFile);

            if (_hashProvider == null)
                _hashProvider = BinaryStorageHashProvider.Default.Value;

            if (_compressionProvider == null)
                _compressionProvider = BinaryStorageCompressionProvider.Default.Value;

            _ongoingRecords = new ConcurrentDictionary<string, string>();
        }

        public void Add(string key, Stream data, StreamInfo parameters)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            if (data == null)
                throw new ArgumentNullException(nameof(data));

            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));

            if (_indexStorage.Contains(key))
                throw new ArgumentException(Resources.ExceptionsResource_AddingIndexRecordDuplicate);

            if (!_ongoingRecords.TryAdd(key, key))
                throw new ArgumentException(Resources.ExceptionsResource_AddingIndexRecordDuplicate);

            if (parameters.Length != null && parameters.Length.Value != data.Length)
                throw new ArgumentException(Resources.ExceptionsResource_LengthDoesNotMatch);

            var hash = _hashProvider.ComputeHash(data);

            if (parameters.Hash != null && !hash.SequenceEqual(parameters.Hash))
                throw new ArgumentException(Resources.ExceptionsResource_HashDoesNotMatch);

            data.Seek(0, SeekOrigin.Begin);

            try
            {
                using (var storageStream = new FileStream(_storageFilePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
                {
                    var record = PersistToStorage(storageStream, data, parameters);

                    record.Hash = hash;

                    _indexStorage.Write(key, record);
                }
            }
            catch (Exception ex)
            {
                if(ex.IsDiskFull())
                    throw new InvalidOperationException(Resources.ExceptionsResource_NotEnoughDiskSpace, ex);

                throw;
            }
            finally
            {
                _ongoingRecords.TryRemove(key, out key);
            }
        }

        public Stream Get(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            var indexRecord = _indexStorage.Read(key);

            if (indexRecord == null)
                throw new KeyNotFoundException(string.Format(Resources.ExceptionsResource_IndexRecordNotFound, key));

            var readStream = GetStorageReadableStream(indexRecord);

            return readStream;
        }

        public bool Contains(string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException(nameof(key));

            return _indexStorage.Contains(key);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _indexStorage?.Dispose();
            }
        }

        private BinaryStorageIndexRecord PersistToStorage(Stream storage, Stream data, StreamInfo parameters)
        {
            Interlocked.CompareExchange(ref _position, storage.Length, 0);

            var compressionLevel = CompressionLevel;

            var alreadyCompressed = parameters != null && parameters.IsCompressed;

            var compressionApplied = compressionLevel != CompressionLevel.NoCompression 
                && data.Length > _compressionThreshold
                && !alreadyCompressed;

            BinaryStorageIndexRecord record;

            if (compressionApplied)
            {
                var tmpFileName = Guid.NewGuid().ToString();
                var tmpFilePath = Path.Combine(_storageTempFilesFolder, tmpFileName);

                FileStream tmpFs = null;

                try
                {
                    tmpFs = new FileStream(tmpFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    using (var compressionStream = _compressionProvider.GetCompressionStream(tmpFs, compressionLevel))
                    {
                        data.CopyTo(compressionStream, _blockSize);
                    }

                    tmpFs.Seek(0, SeekOrigin.Begin);

                    record = ComposeIndexRecord(storage, tmpFs);
                    record.OriginLength = data.Length;
                    record.CompressionLevel = CompressionLevel;

                    return record;
                }
                finally
                {
                    tmpFs?.Dispose();
                    tmpFilePath.DeleteFileIfExists();
                }
            }

            record = ComposeIndexRecord(storage, data);
            record.OriginLength = data.Length;
            record.CompressionLevel = CompressionLevel.NoCompression;

            return record;
        }

        private BinaryStorageIndexRecord ComposeIndexRecord(Stream storage, Stream data)
        {
            EnsureStorageCapacity(data.Length);

            var end = Interlocked.Add(ref _position, data.Length);

            var start = end - data.Length;

            storage.Seek(start, SeekOrigin.Begin);

            data.CopyTo(storage, _blockSize);

            var record = new BinaryStorageIndexRecord
            {
                Length = data.Length,
                Offset = start
            };

            return record;
        }

        private Stream GetStorageReadableStream(BinaryStorageIndexRecord record)
        {
            var storage = new FileStream(_storageFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            if (record.CompressionLevel == CompressionLevel.NoCompression)
                return new SubStream(storage, record.Offset, record.OriginLength);

            storage.Seek(record.Offset, SeekOrigin.Begin);

            var stream = _compressionProvider.GetDecompressionStream(storage);

            var readStream = new SubStream(stream, 0, record.OriginLength);

            return readStream;
        }

        private void EnsureStorageCapacity(long nextFile)
        {
            if (_maxStorageFile == 0)
                return;

            var current = Interlocked.Read(ref _position);

            var isExceeded = current + nextFile >= _maxStorageFile;

            if (isExceeded)
                throw new InvalidOperationException(Resources.ExceptionsResource_StorageCapacityExceeded);
        }
    }
}
