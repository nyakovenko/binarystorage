﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace Zylab.Interview.BinStorage.FileSystem
{
    public static class BinaryStorageFsExtensions
    {
        public static bool IsDiskFull(this Exception ex)
        {
            const int HR_ERROR_HANDLE_DISK_FULL = unchecked((int)0x80070027);
            const int HR_ERROR_DISK_FULL = unchecked((int)0x80070070);

            return ex.HResult == HR_ERROR_HANDLE_DISK_FULL
                || ex.HResult == HR_ERROR_DISK_FULL;
        }

        public static void EnsureDirectoryExists(this string directoryPath)
        {
            var directory = Path.GetFullPath(directoryPath);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        public static void DeleteDirectory(this string directoryPath, bool recursive = true)
        {
            var directory = Path.GetFullPath(directoryPath);

            if (!Directory.Exists(directory))
                return;

            string[] files = Directory.GetFiles(directory);
            string[] dirs = Directory.GetDirectories(directory);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            if (recursive)
            {
                foreach (string dir in dirs)
                {
                    dir.DeleteDirectory();
                }
            }
            

            Directory.Delete(directory, false);
        }

        public static void EnsureFileExists(this string filePath)
        {
            var file = Path.GetFullPath(filePath);

            if (!File.Exists(file))
            {
                File.Create(file).Close();
            }
        }

        public static void DeleteFileIfExists(this string filePath)
        {
            var file = Path.GetFullPath(filePath);

            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public static long GetDirectorySize(this string directoryPath)
        {
            var directory = Path.GetFullPath(directoryPath);

            long directorySize = 0;
            if (!Directory.Exists(directory))
                return directorySize;

            try
            {
                dynamic fso = Activator.CreateInstance(Type.GetTypeFromProgID("Scripting.FileSystemObject"));
                dynamic fldr = fso.GetFolder(directory);
                directorySize = (long)fldr.size;
            }
            catch
            {
                directorySize = GetDirectorySizeByEnumeration(directory);
            }
            
            return directorySize;
        }

        private static long GetDirectorySizeByEnumeration(string directoryPath)
        {
            long directorySize = 0;

            var currentDirectory = new DirectoryInfo(directoryPath);
            currentDirectory.GetFiles().ToList().ForEach(f => directorySize += f.Length);

            currentDirectory.GetDirectories().ToList()
                .ForEach(d => directorySize += GetDirectorySizeByEnumeration(d.FullName));

            return directorySize;
        }
    }
}
