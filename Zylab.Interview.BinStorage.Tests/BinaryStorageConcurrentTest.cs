﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Zylab.Interview.BinStorage.Tests
{
    [TestFixture]
    public class BinaryStorageConcurrentTest : BinaryStorageBaseTest
    {
        [Test, Order(1)]
        public async Task Concurrent_Write_Read_Test()
        {
            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(4)
                .ToList();

            var tasks = new List<Task>();
            foreach (var file in files)
            {
                tasks.Add(Task.Run(() => AssertAddOperation(file)));
            }

            await Task.WhenAll(tasks);
        }

        [Test, Order(2)]
        public void Concurrent_Write_Existent_Key_Test()
        {
            var files = Directory.EnumerateFiles(Registry.SmallFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(1).ToList();

            var file = files.First();
            var tasks = new List<Task>();

            AssertAddOperation(file);

            tasks.Add(Task.Run(() => AssertAddOperation(file)));
            tasks.Add(Task.Run(() => AssertAddOperation(file)));

            Assert.ThrowsAsync<ArgumentException>(async () => await tasks[0]);
        }

        [Test, Order(3)]
        public async Task Concurrent_Contains_Returns_True_If_Record_Exists_Test()
        {
            var files = Directory.EnumerateFiles(Registry.SmallFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(3).ToList();

            var tasks = new List<Task>();

            foreach (var file in files)
            {
                tasks.Add(Task.Run(() => AssertAddOperation(file)));
            }

            await Task.WhenAll(tasks);

            Assert.IsTrue(Storage.Contains(files.First()));
            Assert.IsTrue(Storage.Contains(files.Last()));
        }

        [Test, Order(4)]
        public void Concurrent_Read_Not_Existent_Key_Test()
        {
            var lookups = new[]
            {
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString()
            };

            var tasks = new List<Task>();

            foreach (var l in lookups)
            {
                tasks.Add(Task.Run(() => Storage.Get(l)));
            }

            Assert.ThrowsAsync<KeyNotFoundException>(async () => await tasks[0]);

        }

        [Test, Order(5)]
        public async Task Concurrent_Large_Files_Write_With_Compression_Test()
        {
            Configuration.CompressionThreshold = 500000;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.BigFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(3);

            var tasks = new List<Task>();

            foreach (var file in files)
            {
                tasks.Add(Task.Run(() => AssertAddOperation(file)));
            }

            await Task.WhenAll(tasks);
        }

        [Test, Order(6)]
        public async Task Concurrent_Operations_Are_Generally_Faster_Test()
        {
            Configuration.CompressionThreshold = 500000;

            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories)
                    .Take(5)
                    .ToList();

            var sw = Stopwatch.StartNew();
            Storage = new BinaryStorage(Configuration);
            foreach (var file in files)
            {
                AssertAddOperation(file);
            }
            var sequentialTime = sw.Elapsed;

            Cleanup();

            sw = Stopwatch.StartNew();
            Storage = new BinaryStorage(Configuration);
            var tasks = new List<Task>();
            foreach (var file in files)
            {
                tasks.Add(Task.Run(() => AssertAddOperation(file)));
            }
            await Task.WhenAll(tasks);

            var concurrentTime = sw.Elapsed;

            sw.Stop();

            Assert.IsTrue(concurrentTime < sequentialTime);
        }

        [Test, Order(7)]
        public void Concurrent_Storage_File_Capacity_Exceeded_Test()
        {
            Configuration.CompressionThreshold = 500000;
            Configuration.MaxStorageFile = 5000;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories);

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                var tasks = new List<Task>();

                foreach (var file in files)
                {
                    tasks.Add(Task.Run(() => AssertAddOperation(file)));
                }

                await Task.WhenAll(tasks);
            });
        }

        [Test, Order(8)]
        public void Concurrent_Index_Capacity_Exceeded_Test()
        {
            Configuration.CompressionThreshold = 500000;
            Configuration.MaxIndexFile = 400;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories);

            Assert.ThrowsAsync<InvalidOperationException>(async () =>
            {
                var tasks = new List<Task>();

                foreach (var file in files)
                {
                    tasks.Add(Task.Run(() => AssertAddOperation(file)));
                }

                await Task.WhenAll(tasks);
            });
        }
    }
}
