﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using NUnit.Framework;
using Zylab.Interview.BinStorage.FileSystem;

namespace Zylab.Interview.BinStorage.Tests
{
    public abstract class BinaryStorageBaseTest
    {
        protected IBinaryStorage Storage;
        protected BinaryStorageDirectoryRegistry Registry;
        protected StorageConfiguration Configuration;

        [OneTimeSetUp]
        public virtual void OneTimeSetup()
        {
            BinaryStorageDirectoryRegistry.Default.StorageDirectory.DeleteDirectory();
        }

        [SetUp]
        public virtual void Setup()
        {
            Registry = BinaryStorageDirectoryRegistry.Default;

            var noTestDataFiles = !Directory.Exists(Registry.TestsDataDirectory);

            if (noTestDataFiles)
            {
                Console.WriteLine($@"Please provide some files for tests.");
                Console.WriteLine($@"Put some small files into '{Path.Combine(Registry.TestsDataDirectory, "small-files")}' directory.");
                Console.WriteLine($@"Put some big files into '{Path.Combine(Registry.TestsDataDirectory, "big-files")}' directory.");
                Console.WriteLine($@"Put files of different sizes into '{Path.Combine(Registry.TestsDataDirectory, "different-files")}' directory.");
                Assert.IsFalse(noTestDataFiles, $@"Test data directory '{Path.GetFullPath(Registry.TestsDataDirectory)}' does not exist.");
            }

            Configuration = new StorageConfiguration
            {
                CompressionThreshold = 1000000,
                WorkingFolder = Registry.StorageDirectory
            };

            Storage = new BinaryStorage(Configuration);
        }

        [TearDown]
        public virtual void Cleanup()
        {
            var storageRootDirectory = BinaryStorageDirectoryRegistry.Default.StorageDirectory;

            var storageDirectory = Path.Combine(storageRootDirectory, "data");
            var indexDirectory = Path.Combine(storageRootDirectory, "objects");

            storageDirectory.DeleteDirectory();
            indexDirectory.DeleteDirectory();

            Storage?.Dispose();
        }

        protected class BinaryStorageDirectoryRegistry
        {
            public static readonly BinaryStorageDirectoryRegistry Default = new BinaryStorageDirectoryRegistry();

            private readonly string _testsDataDirectory;

            internal BinaryStorageDirectoryRegistry()
            {
                var currentDirectory = TestContext.CurrentContext.TestDirectory;
                var rootTestsDirectory = Path.Combine(currentDirectory, "..\\..");
                _testsDataDirectory = Path.Combine(rootTestsDirectory, "Data");
                StorageDirectory = Path.Combine(rootTestsDirectory, "Storage");
            }

            public string TestsDataDirectory => _testsDataDirectory;
            public string SmallFilesDirectory => Path.Combine(_testsDataDirectory, "small-files");
            public string RangeOfFilesDirectory => Path.Combine(_testsDataDirectory, "different-files");
            public string BigFilesDirectory => Path.Combine(_testsDataDirectory, "big-files");
            public string StorageDirectory { get; }
        }

        protected void AssertAddOperation(string fileName, StreamInfo streamInfo = null)
        {
            using (var file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                Storage.Add(fileName, file, streamInfo ?? StreamInfo.Empty);

                AsserReadOperation(fileName, file);
            }
        }

        protected void AsserReadOperation(string fileName, FileStream origin = null)
        {
            FileStream file = null;

            try
            {
                file = origin ?? new FileStream(fileName, FileMode.Open, FileAccess.Read);

                file.Seek(0, SeekOrigin.Begin);

                byte[] hash1, hash2;
                using (var resultStream = Storage.Get(fileName))
                using (MD5 md5 = MD5.Create())
                {
                    Assert.IsTrue(file.Length == resultStream.Length);

                    file.Seek(0, SeekOrigin.Begin);

                    hash1 = md5.ComputeHash(file);

                    md5.Initialize();

                    hash2 = md5.ComputeHash(resultStream);
                }

                Assert.IsTrue(hash1.SequenceEqual(hash2));
            }
            finally
            {
                file?.Dispose();
            }
        }
    }
}
