﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
namespace Zylab.Interview.BinStorage.Tests
{
    [TestFixture]
    public class BinaryStorageSequentialTest : BinaryStorageBaseTest
    {
        [Test, Order(1)]
        public void Sequential_Write_Read_Test()
        {
            var files = Directory.EnumerateFiles(Registry.SmallFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(2)
                .ToList();

            foreach (var file in files)
            {
                AssertAddOperation(file);
            }

            AsserReadOperation(files.First());
        }

        [Test, Order(2)]
        public void Sequential_Write_Existent_Key_Test()
        {
            var files = Directory.EnumerateFiles(Registry.SmallFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(1).ToList();

            AssertAddOperation(files.First());
            Assert.Throws<ArgumentException>(() => AssertAddOperation(files.First())); 
        }

        [Test, Order(3)]
        public void Sequential_Contains_Returns_True_If_Record_Exists_Test()
        {
            var files = Directory.EnumerateFiles(Registry.SmallFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(1).ToList();

            AssertAddOperation(files.First());
            
            Assert.IsTrue(Storage.Contains(files.First()));
        }

        [Test, Order(4)]
        public void Sequential_Read_Not_Existent_Key_Test()
        {
            Assert.Throws<KeyNotFoundException>(() => Storage.Get(Guid.NewGuid().ToString()));
        }

        [Test, Order(5)]
        public void Sequential_Large_Files_Write_With_Compression_Test()
        {
            Configuration.CompressionThreshold = 500000;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.BigFilesDirectory, "*", SearchOption.AllDirectories)
                .Take(2);

            foreach (var file in files)
            {
                AssertAddOperation(file);
            }
        }

        [Test, Order(6)]
        public void Sequential_Different_Files_General_Write_Test()
        {
            Configuration.CompressionThreshold = 500000;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                AssertAddOperation(file);
            }
        }

        [Test, Order(7)]
        public void Sequential_Storage_File_Capacity_Exceeded_Test()
        {
            Configuration.CompressionThreshold = 500000;
            Configuration.MaxStorageFile = 5000;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories);

            Assert.Throws<InvalidOperationException>(() =>
            {
                foreach (var file in files)
                {
                    AssertAddOperation(file);
                }
            });
        }

        [Test, Order(8)]
        public void Sequential_Index_Capacity_Exceeded_Test()
        {
            Configuration.CompressionThreshold = 500000;
            Configuration.MaxIndexFile = 400;

            Storage = new BinaryStorage(Configuration);

            var files = Directory.EnumerateFiles(Registry.RangeOfFilesDirectory, "*", SearchOption.AllDirectories);

            Assert.Throws<InvalidOperationException>(() =>
            {
                foreach (var file in files)
                {
                    AssertAddOperation(file);
                }
            });
        }
    }
}
